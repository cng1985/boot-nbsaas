package com.nbsaas.boot.lock.impl;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

// ReentrantLock 工厂实现
public class ReentrantLockFactory implements LockFactory {
    @Override
    public Lock createLock() {
        return new ReentrantLock();
    }
}