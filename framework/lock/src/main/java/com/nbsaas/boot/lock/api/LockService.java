package com.nbsaas.boot.lock.api;

import java.util.concurrent.TimeUnit;

public interface LockService {


    /**
     * 获取锁（无超时）
     *
     * @param key 锁的唯一标识
     * @return 是否成功获得锁
     */
    boolean lock(String key);

    /**
     * 获取锁
     * 
     * @param key 锁的唯一标识
     * @param timeout 锁的超时时间
     * @param unit 超时时间的单位
     * @return 是否成功获得锁
     */
    boolean lock(String key, long timeout, TimeUnit unit);

    /**
     * 释放锁
     * 
     * @param key 锁的唯一标识
     * @return 是否成功释放锁
     */
    boolean unlock(String key);

    /**
     * 判断是否已获取锁
     * 
     * @param key 锁的唯一标识
     * @return 是否已经获取锁
     */
    boolean isLocked(String key);
}
