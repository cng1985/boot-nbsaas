package com.nbsaas.boot.lock.api;

public interface CountingLockService extends LockService {

    /**
     * 获取指定锁的使用次数
     * @param key 锁的唯一标识
     * @return 锁的使用次数
     */
    int getLockCount(String key);

    /**
     * 获取所有锁的总使用次数
     * @return 总使用次数
     */
    int getTotalLockCount();

    /**
     * 获取当前存在的锁数量
     * @return 当前锁的数量
     */
    int getCurrentLockCount();
}
