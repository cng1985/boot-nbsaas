package com.nbsaas.boot.lock.impl;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

// 锁工厂接口
public interface LockFactory {
    Lock createLock();
}


