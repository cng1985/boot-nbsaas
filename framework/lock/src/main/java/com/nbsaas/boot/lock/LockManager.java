package com.nbsaas.boot.lock;

import com.nbsaas.boot.lock.impl.MemoryLockService;
import com.nbsaas.boot.lock.impl.ReentrantLockCountingService;
import com.nbsaas.boot.lock.impl.ReentrantLockService;

public class LockManager {

    // 单例模式，保证LockManager只有一个实例
    private static volatile ReentrantLockCountingService instance;


    private static volatile MemoryLockService memoryLockService;

    private static volatile ReentrantLockService reentrantLockService;


    // 私有化构造方法，确保外部不能直接实例化
    private LockManager() {
    }

    public static ReentrantLockCountingService getLockService() {
        if (instance == null) {
            synchronized (LockManager.class) {
                if (instance == null) {
                    instance = new ReentrantLockCountingService();
                }
            }
        }
        return instance;
    }


    public static MemoryLockService getMemoryLockService() {
        if (memoryLockService == null) {
            synchronized (LockManager.class) {
                if (memoryLockService == null) {
                    memoryLockService = new MemoryLockService();
                }
            }
        }
        return memoryLockService;
    }

    public static ReentrantLockService getReentrantLockService() {
        if (reentrantLockService == null) {
            synchronized (LockManager.class) {
                if (reentrantLockService == null) {
                    reentrantLockService = new ReentrantLockService();
                }
            }
        }
        return reentrantLockService;
    }
}
