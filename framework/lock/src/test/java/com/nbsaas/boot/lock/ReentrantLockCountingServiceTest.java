package com.nbsaas.boot.lock;

import static org.junit.jupiter.api.Assertions.*;

import com.nbsaas.boot.lock.impl.ReentrantLockCountingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.concurrent.*;

public class ReentrantLockCountingServiceTest {

    private ReentrantLockCountingService lockService;

    @BeforeEach
    public void setUp() {
        lockService = new ReentrantLockCountingService();
    }

    @Test
    public void testLockAndUnlockSingleThread() {
        // 在单线程环境下进行测试
        String key = "testKey";

        // 第一次锁定
        assertTrue(lockService.lock(key));
        assertEquals(1, lockService.getLockCount(key));

        // 第二次锁定
        assertFalse(lockService.lock(key));
        assertEquals(1, lockService.getLockCount(key));

        // 解锁
        assertTrue(lockService.unlock(key));
        assertEquals(0, lockService.getLockCount(key));

        // 再次解锁
        assertFalse(lockService.unlock(key));
        assertEquals(0, lockService.getLockCount(key));

        // 确保总锁定次数也正确
        assertEquals(1, lockService.getTotalLockCount());
    }

    @Test
    public void testLockAndUnlockMultipleThreads() throws InterruptedException {
        String key = "testKey";
        int threadCount = 10;
        ExecutorService executorService = Executors.newFixedThreadPool(threadCount);

        // 使用 CountDownLatch 来等待所有线程完成
        CountDownLatch latch = new CountDownLatch(threadCount);
        lockService.lock(key);

        // 启动多个线程并发进行锁定
        for (int i = 0; i < threadCount; i++) {
            executorService.submit(() -> {
                try {
                    // 锁定操作
                    assertEquals(1, lockService.getLockCount(key)); // 每次锁定的 count 应为 1
                    lockService.unlock(key);
                } finally {
                    latch.countDown();
                }
            });
        }

        latch.await(); // 等待所有线程完成
        executorService.shutdown();
        // 验证总锁定次数
        assertEquals(1, lockService.getTotalLockCount());

    }

    @Test
    public void testMultipleLocksAndUnlocks() throws InterruptedException {
        String key = "testKey";
        int threadCount = 5;
        ExecutorService executorService = Executors.newFixedThreadPool(threadCount);

        // 使用 CountDownLatch 来等待所有线程完成
        CountDownLatch latch = new CountDownLatch(threadCount);

        // 启动多个线程并发进行锁定和解锁
        for (int i = 0; i < threadCount; i++) {
            executorService.submit(() -> {
                try {
                    // 锁定操作
                    lockService.lock(key);
                    assertTrue(lockService.getLockCount(key) > 0);

                    // 解锁操作
                    lockService.unlock(key);
                    assertTrue(lockService.getLockCount(key) >= 0);
                } finally {
                    latch.countDown();
                }
            });
        }

        latch.await(); // 等待所有线程完成
        executorService.shutdown();

        // 验证最后的锁定计数
        assertEquals(0, lockService.getLockCount(key));
        assertTrue(lockService.getTotalLockCount() > 0); // 至少应该有一些锁定和解锁操作
    }
}
