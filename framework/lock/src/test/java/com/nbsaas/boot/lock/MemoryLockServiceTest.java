package com.nbsaas.boot.lock;

import com.nbsaas.boot.lock.api.LockService;
import com.nbsaas.boot.lock.impl.MemoryLockService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

public class MemoryLockServiceTest {

    private LockService lockService;

    @BeforeEach
    public void setUp() {
        lockService =LockManager.getLockService();
    }

    @Test
    public void testLockAndUnlock() {
        String key = "testKey";

        // 尝试获取锁
        assertTrue(lockService.lock(key));
        //assertFalse(lockService.lock(key));
        // 检查锁状态
        assertTrue(lockService.isLocked(key));

        // 释放锁
        assertTrue(lockService.unlock(key));

        // 再次检查锁状态
        assertFalse(lockService.isLocked(key));
    }

    @Test
    public void testDoubleLock() throws InterruptedException {
        String key = "testKey";

        // 尝试获取锁
        assertTrue(lockService.lock(key));

        // 启动另一个线程尝试获取相同的锁
        Thread otherThread = new Thread(() -> {
            assertFalse(lockService.lock(key)); // 锁已经被占用，应该返回false
        });
        otherThread.start();
        otherThread.join();  // 等待线程结束

        // 解锁
        assertTrue(lockService.unlock(key));
    }

    @Test
    public void testMultiThreadLock() throws InterruptedException {
        String key = "testKey";

        // 启动多个线程尝试获取锁
        int threadCount = 10;
        CountDownLatch latch = new CountDownLatch(threadCount); // 用于等待所有线程完成
        Runnable lockTask = () -> {
            try {
                // 尝试获取锁，失败则等待
                boolean locked = lockService.lock(key);
                assertTrue(locked);
                // 确保解锁后，锁被正确释放
                assertTrue(lockService.unlock(key));
            } finally {
                latch.countDown();  // 完成后减少计数
            }
        };

        for (int i = 0; i < threadCount; i++) {
            new Thread(lockTask).start();
        }

        latch.await();  // 等待所有线程执行完毕
    }

    @Test
    public void testLockWithTimeout() throws InterruptedException {
        String key = "testKey";

        // 尝试获取锁
        assertTrue(lockService.lock(key));

        // 启动另一个线程尝试在超时后获取相同的锁
        Thread otherThread = new Thread(() -> {
            try {
                // 超时后仍然无法获得锁
                assertFalse(lockService.lock(key, 100, TimeUnit.MILLISECONDS));
            } catch (Exception e) {
                fail("Exception occurred: " + e.getMessage());
            }
        });
        otherThread.start();
        otherThread.join();  // 等待线程结束

        // 解锁
        assertTrue(lockService.unlock(key));
    }

    @Test
    public void testUnlockWithoutLock() {
        String key = "testKey";

        // 尝试解锁一个没有锁的key
        assertFalse(lockService.unlock(key));
    }

    @Test
    public void testLockTimeout() throws InterruptedException {
        String key = "testKey";

        // 尝试获取锁
        assertTrue(lockService.lock(key, 200, TimeUnit.MILLISECONDS));

        // 启动另一个线程，尝试获取锁并检查超时
        Thread otherThread = new Thread(() -> {
            // 锁定时间超过200ms，第二次应该返回false
            assertFalse(lockService.lock(key, 100, TimeUnit.MILLISECONDS));
        });
        otherThread.start();
        otherThread.join();

        // 释放锁
        assertTrue(lockService.unlock(key));
    }
}
