package com.nbsaas.boot.lock;

import com.nbsaas.boot.lock.api.LockService;
import com.nbsaas.boot.lock.impl.StampedLockService;
import org.junit.jupiter.api.Test;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;

class StampedLockServiceTest {

    private final LockService lockService = new StampedLockService();

    @Test
    void testLockInMultipleThreads() throws InterruptedException {
        String key = "testKey";
        int threadCount = 10;  // 模拟 10 个线程同时尝试加锁

        // 使用 CountDownLatch 来确保所有线程都开始竞争锁
        CountDownLatch latch = new CountDownLatch(threadCount);
        AtomicInteger successCount = new AtomicInteger(0);

        // 启动多个线程同时尝试加锁
        ExecutorService executor = Executors.newFixedThreadPool(threadCount);
        for (int i = 0; i < threadCount; i++) {
            executor.submit(() -> {
                latch.countDown(); // 线程准备好，减少计数
                try {
                    latch.await(); // 等待所有线程就绪
                    // 线程开始加锁
                    if (lockService.lock(key)) {
                        successCount.incrementAndGet(); // 成功加锁，记录
                    }
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            });
        }

        executor.shutdown();
        executor.awaitTermination(10, TimeUnit.SECONDS);

        // 只有一个线程能成功加锁
        assertEquals(1, successCount.get(), "只有一个线程能成功加锁");

        // 其他线程应该无法加锁
        assertTrue(lockService.isLocked(key), "锁应该已经被占用");
    }

    @Test
    void testLockWithTimeLimitAndMultipleThreads() throws InterruptedException {
        String key = "testTimeoutKey";
        int threadCount = 5;  // 模拟 5 个线程

        // 使用 CountDownLatch 来确保所有线程都开始竞争锁
        CountDownLatch latch = new CountDownLatch(threadCount);
        AtomicInteger successCount = new AtomicInteger(0);

        ExecutorService executor = Executors.newFixedThreadPool(threadCount);
        for (int i = 0; i < threadCount; i++) {
            executor.submit(() -> {
                latch.countDown(); // 线程准备好，减少计数
                try {
                    latch.await(); // 等待所有线程就绪
                    // 带超时尝试加锁，最多等待 1 秒
                    if (lockService.lock(key, 1, TimeUnit.SECONDS)) {
                        successCount.incrementAndGet(); // 成功加锁，记录
                    }
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            });
        }

        executor.shutdown();
        executor.awaitTermination(10, TimeUnit.SECONDS);

        // 只有一个线程能成功加锁
        assertEquals(1, successCount.get(), "只有一个线程能成功加锁");

        // 其他线程应该无法加锁
        assertTrue(lockService.isLocked(key), "锁应该已经被占用");

        // 等待超时没有加锁的线程，应该未加锁
    }

    @Test
    void testUnlockAfterThreadTask() throws InterruptedException, ExecutionException {
        String key = "testMultiThreadTimeConsumingTaskKey";
        int threadCount = 5;  // 模拟 5 个线程

        // 使用 CountDownLatch 来确保所有线程开始竞争锁
        CountDownLatch latch = new CountDownLatch(threadCount);
        AtomicInteger successCount = new AtomicInteger(0);

        ExecutorService executor = Executors.newFixedThreadPool(threadCount);
        for (int i = 0; i < threadCount; i++) {
            executor.submit(() -> {
                latch.countDown(); // 线程准备好，减少计数
                try {
                    latch.await(); // 等待所有线程就绪
                    // 每个线程尝试加锁并执行耗时任务
                    if (lockService.lock(key)) {
                        try {
                            // 模拟耗时任务
                            Thread.sleep(1000);
                            successCount.incrementAndGet();
                        } catch (InterruptedException e) {
                            Thread.currentThread().interrupt();
                        } finally {
                            lockService.unlock(key); // 完成后释放锁
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }

        executor.shutdown();
        executor.awaitTermination(10, TimeUnit.SECONDS);

        // 至少有一个线程会成功执行耗时任务并加锁
        assertTrue(successCount.get() > 0, "至少有一个线程成功加锁并执行任务");

        // 等待所有线程结束后锁应该已被释放
        Thread.sleep(500); // 等待所有线程解锁
        assertFalse(lockService.isLocked(key), "锁应该已经被释放");
    }

    @Test
    void testMultipleThreadsWithTimeConsumingTask() throws InterruptedException {
        String key = "testMultiThreadTimeConsumingTaskKey";
        int threadCount = 5;  // 模拟 5 个线程

        // 使用 CountDownLatch 来确保所有线程开始竞争锁
        CountDownLatch latch = new CountDownLatch(threadCount);
        AtomicInteger successCount = new AtomicInteger(0);

        ExecutorService executor = Executors.newFixedThreadPool(threadCount);
        for (int i = 0; i < threadCount; i++) {
            executor.submit(() -> {
                latch.countDown(); // 线程准备好，减少计数
                try {
                    latch.await(); // 等待所有线程就绪
                    // 每个线程尝试加锁并执行耗时任务
                    if (lockService.lock(key)) {
                        // 模拟耗时任务
                        Thread.sleep(1000);
                        successCount.incrementAndGet();
                        lockService.unlock(key); // 完成后释放锁
                    }
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            });
        }

        //executor.shutdown();
        executor.awaitTermination(10, TimeUnit.SECONDS);

        // 至少有一个线程会成功执行耗时任务并加锁
        assertTrue(successCount.get() > 0, "至少有一个线程成功加锁并执行任务");
        // 在所有线程结束后，锁应该已被释放
        Thread.sleep(500); // 等待所有线程解锁
        assertFalse(lockService.isLocked(key), "锁应该已经被释放");
    }
}
