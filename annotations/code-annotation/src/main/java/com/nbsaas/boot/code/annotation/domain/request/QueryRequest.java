package com.nbsaas.boot.code.annotation.domain.request;


import lombok.Data;

@Data
public class QueryRequest {

    private String className;

    private String alias;
}
